import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel, CommentReactionModel }
  from '../models/index';
import BaseRepository from './baseRepository';

const { Op } = require('sequelize');

// const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId
    } = filter;

    const where = {};
    if (userId) {
      Object.assign(where, { userId });
    }
    Object.assign(where, { deletedAt: { [Op.is]: null } });

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "postReactions" as "pR"
                        WHERE "post"."id" = "pR"."postId" AND "pR"."isLike" = true)`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "postReactions" as "pR"
                        WHERE "post"."id" = "pR"."postId" AND "pR"."isLike" = false)`), 'dislikeCount']
          // Not correct work with grouping by 'postReactions.id', 'postReactions->user.id',
          // thats why I have to change next:
          // [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          // [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [
        {
          model: PostReactionModel,
          attributes: ['id', 'isLike'],
          include: {
            model: UserModel,
            attributes: ['id', 'username']
          }
        }, {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: ImageModel,
          attributes: ['id', 'link']
        }, {
          model: PostReactionModel,
          attributes: [],
          duplicating: false
        }],
      group: [
        'post.id',
        'postReactions.id',
        'postReactions->user.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "postReactions" as "pR"
                        WHERE "post"."id" = "pR"."postId" AND "pR"."isLike" = true)`), 'likeCount'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "postReactions" as "pR"
                        WHERE "post"."id" = "pR"."postId" AND "pR"."isLike" = false)`), 'dislikeCount']
        ]
      },
      include: [
        {
          model: CommentModel,
          where: { deletedAt: { [Op.is]: null } },
          required: false,
          attributes: {
            include: [
              [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "commentReactions" as "CR"
                        WHERE "isLike" = true AND "comments"."id" = "CR"."commentId")`), 'likeCount'],
              [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "commentReactions" as "CR"
                        WHERE "isLike" = false AND "comments"."id" = "CR"."commentId")`), 'dislikeCount']
            ]
          },
          include: [{
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }, {
            model: CommentReactionModel,
            attributes: ['id', 'isLike'],
            include: {
              model: UserModel,
              attributes: ['id', 'username']
            }
          }, {
            model: CommentReactionModel,
            attributes: []
          }]
        }, {
          model: PostReactionModel,
          attributes: ['id', 'isLike'],
          include: {
            model: UserModel,
            attributes: ['id', 'username']
          }
        }, {
          model: UserModel,
          attributes: ['id', 'username', 'email'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: ImageModel,
          attributes: ['id', 'link']
        }, {
          model: PostReactionModel,
          attributes: []
        }]
    });
  }

  softDeletePost(id) {
    const now = new Date();
    return this.model.update({
      deletedAt: now
    }, {
      where: { id }
    });
  }
}

export default new PostRepository(PostModel);
