import env from '../env';

export const { emailServiceId, emailTemplateId, emailUserId } = env.emailJS;
