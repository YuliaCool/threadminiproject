import { Router } from 'express';
import * as postService from '../services/postService';
import * as emailService from '../services/emailService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => {
      res.send(posts);
    })
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post by message on the screen
        if (req.body.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');

          // notify a user if someone (not himself) liked his post by email
          const userLiked = req.user.username;
          const { referer } = req.headers;
          const { postId } = req.body;
          emailService.sendEmail(userLiked, referer, postId);
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/delete', (req, res, next) => postService.softDelete(req.body)
    .then(result => res.send(result))
    .catch(next));

export default router;
