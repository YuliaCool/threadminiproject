import axios from 'axios';
import * as emailConfig from '../../config/emailJSConfig';
import * as postService from './postService';

const dataEmail = {
  service_id: emailConfig.emailServiceId,
  template_id: emailConfig.emailTemplateId,
  user_id: emailConfig.emailUserId,
  template_params: {
    authorEmail: '',
    userLiked: '',
    postLink: ''
  }
};

const sendEmailJS = async () => {
  try {
    await axios.post(
      'https://api.emailjs.com/api/v1.0/email/send',
      JSON.stringify(dataEmail),
      { headers: { 'Content-Type': 'application/json' } }
    );
  } catch ({ response: { data: { status, data } } }) {
    throw new Error(status, data);
  }
};

export const sendEmail = async (userLiked, referer, postId) => {
  dataEmail.template_params.userLiked = userLiked;
  dataEmail.template_params.postLink = `${referer}share/${postId}`;
  const author = await postService.getAuthorPost(postId);
  dataEmail.template_params.authorEmail = author.email;
  await sendEmailJS();
};
