import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  // let changedReaction;

  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike })
    /*  .then(result => {
        // this function checkes if user changes reaction (set like when dislike is already clicked and vice versa)
        changedReaction = true;
        return result;
      }) */
  );

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result)
    ? { id: false }
    : postReactionRepository.getPostReaction(userId, postId);
  /* should be added to this part of return,
     but sending it with result of getPostReaction function wasn't successed for me,
     thats why I check if item in db was updated on client side (in actions.js) by comparing createdAt
     and updatedAt field in response */
};

export const softDelete = async ({ postId }) => {
  const [deleted] = await postRepository.softDeletePost(postId);
  return (Number.isInteger(deleted) && deleted)
    ? { deleted: true }
    : { deleted: false };
};

export const getAuthorPost = async postId => {
  const { user } = await getPostById(postId);
  return user;
};
