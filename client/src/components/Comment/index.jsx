import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import ReactTooltip from 'react-tooltip';

import styles from './styles.module.scss';

const Comment = ({ userId, comment, likeComment, dislikeComment, deleteComment }) => {
  const {
    id,
    likeCount,
    dislikeCount,
    body,
    createdAt,
    user,
    commentReactions
  } = comment;
  const idReactionCommentLike = `${id}-like`;
  const idReactionCommentDisLike = `${id}-dislike`;
  const usersLiked = commentReactions.filter(reaction => reaction.isLike);
  const usersDisliked = commentReactions.filter(reaction => !reaction.isLike);
  const date = moment(createdAt).fromNow();
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata className={styles.metadata}>
          {date}
        </CommentUI.Metadata>
        <CommentUI.Actions className={styles.actionButton}>
          <CommentUI.Action className={styles.changeButtons}>
            <Button
              icon
              size="mini"
              className={user.id === userId ? styles.show : styles.hide}
              onClick={() => { if (user.id === userId) deleteComment(id); }}
            >
              <Icon name="delete" />
            </Button>
          </CommentUI.Action>
        </CommentUI.Actions>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <CommentUI.Action>
            <Label
              basic
              size="small"
              data-tip
              data-for={idReactionCommentLike}
              className={styles.toolbarBtn}
              onClick={() => likeComment(id)}
            >
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
            <ReactTooltip
            // for correct position tooltip in modal window, tooltip should be outside the modal window
              id={idReactionCommentLike}
              place="bottom"
              type="info"
              effect="float"
              tipPointerPosition="Buttom"
            >
              <span className={styles.toolbartip}>
                <ul>
                  { usersLiked.length === 0
                    ? 'Be first who liked this comment!'
                    : usersLiked.map(
                      (likeReaction => (
                        <li key={likeReaction.id}>
                          {likeReaction.user.username}
                        </li>
                      ))
                    )}
                </ul>
              </span>
            </ReactTooltip>
          </CommentUI.Action>
          <CommentUI.Action>
            <Label
              basic
              size="small"
              data-tip
              data-for={idReactionCommentDisLike}
              className={styles.toolbarBtn}
              onClick={() => dislikeComment(id, userId)}
            >
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
            <ReactTooltip id={idReactionCommentDisLike} place="bottom" type="info" effect="float">
              <span className={styles.toolbartip}>
                <ul>
                  { usersDisliked.length === 0
                    ? 'Be first who disliked this comment!'
                    : usersDisliked.map(
                      (dislikeReaction => (
                        <li key={dislikeReaction.id}>
                          {dislikeReaction.user.username}
                        </li>
                      ))
                    )}
                </ul>
              </span>
            </ReactTooltip>
          </CommentUI.Action>
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  userId: PropTypes.string.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default Comment;
