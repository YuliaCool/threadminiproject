import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Icon, Button } from 'semantic-ui-react';
import validator from 'validator';
import emailjs from 'emailjs-com';

import styles from './styles.module.scss';

const SharedPostEmail = ({ postId, close }) => {
  const { EMAIL_SERVICE_USERID, SERVICE_ID, TEMPLATE_ID } = process.env;
  // let input = useRef();

  // emailjs.send(service_id, template_id, template_params);
  // const emailTemplateParams = {
  //   friendEmail: '',
  //   postLink: ''
  // };

  const [mail, setEmail] = useState('');
  const linkToPost = `${window.location.origin}/share/${postId}`;
  // const checkEmail = e => {
  //   input.select();
  //   e.target.focus();
  //   alert('hello');
  //   console.log(linkToPost);
  // };

  const sendEmailMy = async email => {
    console.log(EMAIL_SERVICE_USERID);
    console.log(SERVICE_ID);
    console.log(TEMPLATE_ID);
    if (!email) {
      return;
    }

    const send = validator.isEmail(email)
      ? emailjs.send(SERVICE_ID, TEMPLATE_ID, {
        friendEmail: email,
        postLink: linkToPost
      }, EMAIL_SERVICE_USERID)
        .then(result => {
          console.log(result.text);
        }, error => {
          console.log(error.text);
        })
      : console.log('send validation');
    console.log(linkToPost);
    console.log(send);
    // await sendEmail({ email });
    setEmail('');
  };

  function sendEmail(e, email) {
    const isEmail = validator.isEmail(email);
    console.log(isEmail);
    e.preventDefault();
  }
  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        <span>
          <Icon color="green" name="mail" />
          Send
        </span>
      </Modal.Header>
      <Modal.Content>
        <Form reply onSubmit={sendEmailMy}>
          <Form.Input
            value={mail}
            placeholder="Type a email..."
            onChange={ev => sendEmail(ev.target.value)}
          />
          <Button type="submit" content="Send" labelPosition="left" icon="mail" primary />
        </Form>
      </Modal.Content>
    </Modal>
  );
};

SharedPostEmail.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostEmail;
