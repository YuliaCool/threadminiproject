import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Button } from 'semantic-ui-react';
import moment from 'moment';
import ReactTooltip from 'react-tooltip';

import styles from './styles.module.scss';

const Post = ({ userId, post, likePost, dislikePost, toggleExpandedPost, sharePost, deletePost }) => {
  const {
    id,
    image,
    body,
    user,
    postReactions,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const idReactionPostLike = `${id}-like`;
  const idReactionPostDisLike = `${id}-dislike`;
  const usersLiked = postReactions.filter(reaction => reaction.isLike);
  const usersDisliked = postReactions.filter(reaction => !reaction.isLike);
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta className={styles.metadata}>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          <span className={styles.changeButtons}>
            <Button
              icon
              size="mini"
              className={user.id === userId ? styles.show : styles.hide}
              onClick={() => { if (user.id === userId) deletePost(id); }}
            >
              <Icon name="delete" />
            </Button>
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          data-tip
          data-for={idReactionPostLike}
          className={styles.toolbarBtn}
          onClick={() => likePost(id)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <ReactTooltip id={idReactionPostLike} place="left" type="info" effect="float">
          <span className={styles.toolbartip}>
            <ul>
              { usersLiked.length === 0
                ? 'Be first who liked this post!'
                : usersLiked.map(
                  (likeReaction => (
                    <li key={likeReaction.id}>
                      {likeReaction.user.username}
                    </li>
                  ))
                )}
            </ul>
          </span>
        </ReactTooltip>
        <Label
          basic
          size="small"
          as="a"
          data-tip
          data-for={idReactionPostDisLike}
          className={styles.toolbarBtn}
          onClick={() => dislikePost(id)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <ReactTooltip id={idReactionPostDisLike} place="right" type="info" effect="float">
          <span className={styles.toolbartip}>
            <ul>
              { usersDisliked.length === 0
                ? 'Be first who dislked this post!'
                : usersDisliked.map(
                  (dislikeReaction => (
                    <li key={dislikeReaction.id}>
                      {dislikeReaction.user.username}
                    </li>
                  ))
                )}
            </ul>
          </span>
        </ReactTooltip>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
