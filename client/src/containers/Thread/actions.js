import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
// import { v4 as uuidv4 } from 'uuid';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  console.log(post);
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId);
  const reactionWasChanged = (createdAt && updatedAt)
    ? !(createdAt === updatedAt)
    : null;
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  // if user set like whe he has already disliked (and vice versa), count of dislikes should be reduced:
  const dislikeDiff = (reactionWasChanged) ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  // get actual information about who liked this post for ReactTooltip
  const allPosts = await postService.getAllPosts();
  dispatch(setPostsAction(allPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));

    // get actual information about who liked this post for ReactTooltip
    const post = await postService.getPost(postId);
    dispatch(setExpandedPostAction(post));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.dislikePost(postId);
  const reactionWasChanged = (createdAt && updatedAt)
    ? !(createdAt === updatedAt)
    : null;
  const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  // if user set dislike whe he has already liked (and vice versa), count of likes should be reduced:
  const likeDiff = (reactionWasChanged) ? -1 : 0;

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(post.likeCount) + likeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  // get actual information about who disliked this post for ReactTooltip
  const allPosts = await postService.getAllPosts();
  dispatch(setPostsAction(allPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));

    // get actual information about who disliked this post for ReactTooltip
    const post = await postService.getPost(postId);
    dispatch(setExpandedPostAction(post));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.likeComment(commentId);
  const comment = await commentService.getComment(commentId);
  const reactionWasChanged = (createdAt && updatedAt)
    ? !(createdAt === updatedAt)
    : null;
  const diff = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  // if user set like whe he has already disliked (and vice versa), count of dislikes should be reduced:
  const dislikeDiff = (reactionWasChanged) ? -1 : 0;

  const mapLikes = currentComment => ({
    ...currentComment,
    likeCount: Number(currentComment.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(currentComment.dislikeCount) + dislikeDiff
  });

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();

  const updatedComments = expandedPost.comments.map(currentComment => (currentComment.id !== commentId
    ? currentComment
    : mapLikes(currentComment)));
  expandedPost.comments = updatedComments;

  const updatedPosts = posts.map(post => (post.id !== expandedPost.id
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updatedPosts));
  // dispatch(setExpandedPostAction(expandedPost));
  // get actual information about who liked this comment for ReactTooltip
  const post = await postService.getPost(expandedPost.id);
  dispatch(setExpandedPostAction(post));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.dislikeComment(commentId);
  const comment = await commentService.getComment(commentId);
  const reactionWasChanged = (createdAt && updatedAt)
    ? !(createdAt === updatedAt)
    : null;
  const diff = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed
  // if user set like whe he has already disliked (and vice versa), count of dislikes should be reduced:
  const likeDiff = (reactionWasChanged) ? -1 : 0;

  const mapLikes = currentComment => ({
    ...currentComment,
    likeCount: Number(currentComment.likeCount) + likeDiff, // diff is taken from the current closure
    dislikeCount: Number(currentComment.dislikeCount) + diff
  });

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();

  const updatedComments = expandedPost.comments.map(currentComment => (currentComment.id !== commentId
    ? currentComment
    : mapLikes(currentComment)));
  expandedPost.comments = updatedComments;

  const updatedPosts = posts.map(post => (post.id !== expandedPost.id
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updatedPosts));

  // get updated information about who disliked this comment for ReactTooltip from db:
  const post = await postService.getPost(expandedPost.id);
  dispatch(setExpandedPostAction(post));

  // or counting information about who disliked this comment for ReactTooltip from first handling to db
  // with store Redux using:
  //
  // const updatedCommentReaction = expandedPost.comments.map(reactedComment => (reactedComment.id !== reactedComment)
  //   // if id of current commentReaction of selected comment in db is exist, dislike was added,
  //   // and maybe it was changed from like
  //   ? (id
  //       ? reactionWasChanged
  //         // if dislike was changes from like, we should update isLike in current commentReaction by id:
  //         ? reactedComment.commentReactions.map(dl => (dl.id === id) ? dl.isLike = false : dl)
  //         // if dislike was added, we should add to new item to commentReaction array of current comment:
  //         : reactedComment.commentReactions.push({
  //           id: uuidv4(), // IS IT A GOOD IDEA TO CREATE FAKED ID FROM HERE?
  //           isLike: false,
  //           user: {
  //             id: connect(mapStateToProps)(user.id), // get id from Context
  //             username: connect(mapStateToProps)(user.username) // get username from Context
  //           }
  //         })
  //        // if id is null, dislike was deleted and like wasn't added,
  //        // so we should delete this commentReaction from this comment
  //       : reactedComment.commentReactions.map(
  //         dislike => (dislike.user.id === disappointedUserId) // if old comment Reaction of user is found, delete ir
  //                     ? reactedComment.commentReactions.splice(reactedComment.commentReactions.indexOf(dislike),1)
  //                     : dislike)
  //     )
  //   : reactedComment);
  // expandedPost.comments.commentReactions = updatedCommentReaction;
  // dispatch(setExpandedPostAction(expandedPost));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { deleted } = await postService.deletePost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  if (deleted) {
    const updated = posts.filter(post => !(post.id === postId)).map(post => post);

    dispatch(setPostsAction(updated));
    if (expandedPost && expandedPost.id === postId) {
      dispatch(setExpandedPostAction(null));
    }
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { deleted } = await commentService.deleteComment(commentId);
  const { posts: { posts, expandedPost } } = getRootState();
  if (deleted) {
    const updatedComments = expandedPost.comments
      .filter(comment => !(comment.id === commentId))
      .map(comment => comment);

    const mapComments = post => ({
      ...post,
      comments: updatedComments,
      commentCount: Number(post.commentCount) - 1
    });

    const updatedPosts = posts.map(post => (post.id !== expandedPost.id
      ? post
      : mapComments(post)));

    const updatedCurrentPost = mapComments(expandedPost);

    dispatch(setPostsAction(updatedPosts));
    dispatch(setExpandedPostAction(updatedCurrentPost));
  }
};
